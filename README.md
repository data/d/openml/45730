# OpenML dataset: cardiotocography

https://www.openml.org/d/45730

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The dataset consists of measurements of fetal heart rate and uterine contraction features on cardiotocograms classified by expert obstetricians.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45730) of an [OpenML dataset](https://www.openml.org/d/45730). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45730/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45730/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45730/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

